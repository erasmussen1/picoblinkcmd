#include "pico/stdlib.h"
#include <stdio.h>
#include <ctype.h>
#include <string.h>

#include "bmenu.h"
#include "menu.h"
#include "tst.h"


#define DASHES 21


CMD* ME_find_command(const CMD* m, char ch);
unsigned short ME_process_menu(char* cmdbuf, int index);
void ME_display_menu(const char* topname, const CMD* m);
void ME_display_menu_entry(const char* topname, const CMD* p);
unsigned short ME_display_item(ITEM_LIST* p);
CMDERR* ME_find_error_entry(long errcode);

void ME_remove_start_blank(char* str);


const CMDERR ME_CmdErrTable[] = {{ERR_BADPARM, "Bad command parameters!"},
                                 {ERR_OUT_OF_RANGE_PARM, "Out of range!"},
                                 {ERR_BADCMD, "Unrecognized command!"},
                                 {ERR_CMDCONFLICT, "Command conflict Disabled!"},
                                 {ERR_RECDEPOPEN, "COMMAND MUST BE SET TO 0 TO ENABLE RECORDER"},
                                 {ERR_RECNOEXIST, "RECORDER DOES NOT EXIST"},
                                 {0, NULL}};


const CMD main_cmd_table[] = {{"B", 0, (CMD*)BM_menu_table, 0, "B Commands", NULL},
                              {"?", 1, 0, 0, "Help menu", NULL},
                              {NULL, 0, NULL, 0, NULL, NULL}};

unsigned short cmd_init_flag;
unsigned short warned;
unsigned short valid_cmd;

#define CMDBUFSIZE 128

const unsigned int cmd_buffer_max_size = CMDBUFSIZE;
char cmdbuf[CMDBUFSIZE + 1];
unsigned int cmd_index;


void command() {
    putch('\r');
    putch('\n');
    putch('>');

    unsigned long done = 0;

    cmd_index = 0;
    cmdbuf[0] = '\0';

    unsigned long ch;
    while (!done) {
        ch = getch();
        ch &= 0xFF;

        switch (ch) {
            case '\r':
            case '\n':
                if (cmd_index < cmd_buffer_max_size) {
                    cmdbuf[cmd_index++] = '\0';
                } else {
                    cmdbuf[cmd_buffer_max_size] = '\0';
                }
                done = 1;
                break;

            case ESC:
                cmd_index = 0;
                putch('\r');
                putch('\n');
                putch('>');
                break;

            case BS:
                if (cmd_index > 0) {
                    cmd_index--;
                    putch(BS);
                    putch(' ');
                    putch(BS);
                }
                break;

            default:
                if ((ch >= ' ') && (ch <= 'z')) {
                    if ((ch == ' ') && (cmd_index < 2)) {
                        putch(ch);
                    } else if (cmd_index < cmd_buffer_max_size) {
                        cmdbuf[cmd_index++] = ch;
                        putch(ch);
                    }
                }
                break;
        }

        sleep_ms(2);
    }

    int rc = ME_process_menu(cmdbuf, cmd_index);
    if (rc) {
        ME_command_error(ERR_BADCMD, NULL);
        cmd_index = 0;
        cmdbuf[0] = '\0';
    }
}

CMD* ME_find_command(const CMD* m, char ch) {
    const CMD* cmd = NULL;
    const CMD* p = m;

    while (p->name) {
        if (toupper(ch) == p->name[0]) {
            cmd = p;
            break;
        }
        p++;
    }

    return ((CMD*)cmd);
}

unsigned short ME_process_menu(char* cmdbuf, int cmd_index) {
    const CMD* p = NULL;
    const CMD* prev = NULL;
    const CMD* m = (CMD*)main_cmd_table;

    if (cmd_index < 2) {
        return 0;
    }

    for (int i = 0; i < cmd_index; i++) {
        prev = p;

        p = ME_find_command(m, cmdbuf[i]);
        if (!p) {
            return 1;
        }

        if (*p->name == '?') {
            if (prev) {
                ME_display_menu(prev->name, m);
            } else {
                ME_display_menu("", m);
            }

            return 0;
        }

        if (p->submenu) {
            m = p->submenu;
            continue;
        }

        char* q = &cmdbuf[i + 1];
        while (*q && (*q == ' ')) {
            q++;
        }

        if (*q == '?') {
            printf("\n");

            if (prev) {
                ME_display_menu_entry(prev->name, p);
            } else {
                ME_display_menu_entry("", p);
            }
            printf("\n");

            return 0;
        }

        if (p->func) {
            (p->func)(cmdbuf);
            return 0;
        }
    }

    return 1;
}

void ME_display_menu(const char* topname, const CMD* m) {
    const CMD* p = NULL;

    printf("\n2021 (C) Blinker inc.\n");
    printf("\nAvailable Commands:\n");

    if (strcmp(topname, "")) {
        printf("\n");
    }

    for (p = m; p->name != 0; p++) {
        ME_display_menu_entry(topname, p);
        printf("\n");
    }
}

void ME_display_menu_entry(const char* topname, const CMD* p) {
    ITEM_LIST* item = NULL;

    if (*topname) {
        printf("%s%s = ", topname, p->name);
    } else {
        printf("%s? ", p->name);
    }

    int len = DASHES;

    if (p->item_list != 0) {
        for (item = p->item_list; item->data_type != ITEM_LAST; item++) {
            len -= ME_display_item(item);
        }

        len--;
        putch(' ');
    }

    if (len > 0) {
        for (int i = 0; i < len; i++) {
            putch('-');
        }
    }

    printf(" %s", p->descrip);
}

unsigned short ME_display_item(ITEM_LIST* p) {
    unsigned long float_flag;
    long lword;

    float_flag = 0;

    switch (p->data_type) {
        case ITEM_CHAR:
            lword = (long)*((char*)p->address);
            break;

        case ITEM_STR:
            lword = (long)((char*)p->address);
            break;

        case ITEM_SHORT:
            lword = (long)*((short*)p->address);
            break;

        case ITEM_INT:
            lword = (long)*((int*)p->address);
            break;

        case ITEM_LONG:
            lword = *((long*)p->address);
            break;

        case ITEM_BYTE_LIST: {
            char* temp = (char*)p->address;

            float_flag = 0;

            while (*temp != 0x00) {
                if ((float_flag + p->field_len + 1) >= DASHES) {
                    printf("\n   ");
                    float_flag = 0;
                }

                printf(p->fmt, *temp++);
                printf(",");
                float_flag += 4;
            }

            putch(0x08); /* backspace */
            putch(0x20); /* space, to clear last comma */
            putch(0x08); /* backspace */
            return ((short)(float_flag - 1));
            break;
        }

        case ITEM_SHORT_BIN: {
            short data = (short)*((short*)p->address);
            short index;

            for (index = p->field_len; index >= 1; index--) {
                if ((1 << (index - 1)) & data) {
                    putch(0x31);
                } else {
                    putch(0x30);
                }
            }

            return (p->field_len);
            break;
        }
    }

    if (p->mask) {
        lword &= p->mask;
    }

    if (p->shift) {
        lword >>= p->shift;
    }

    switch (float_flag) {
        case 0:
            printf(p->fmt, lword);
            break;

        case 1:
            printf(p->fmt, lword);
            break;

        default:

            break;
    }

    return (p->field_len);
}

CMDERR* ME_find_error_entry(long errcode) {
    const CMDERR* p = NULL;

    for (p = ME_CmdErrTable; p->msg != NULL; p++) {
        if (p->code == errcode) {
            break;
        }
    }

    return ((CMDERR*)p);
}

void ME_command_error(long errcode, char* errstr) {
    const CMDERR* p = ME_find_error_entry(errcode);

    if (p) {
        printf("\nERR: %s\n", p->msg);
    } else {
        printf("\nERR: Unknown command error!%s\n", ((errstr != 0) ? errstr : ""));
    }
}

void ME_remove_start_blank(char* str) {
    int cn = 0;
    unsigned int flag = 1;
    char ch = 0;

    for (int c = 0; c < (CMDBUFSIZE - 1); c++) {
        ch = str[c];
        /* Remove space from beginning of string. */
        if ((ch == ' ') && (cn == 0)) {
            continue;
        }

        if ((ch == '\r' || ch == '\n') || ch == '\0') {
            break;
        }

        if ((ch == '=') || (ch == ',') || (ch == ';')) {
            ch = ' ';
        }

        str[cn] = ch;

        if (ch == ' ') {
            if (flag) {
                flag = 0;
                cn++;
            }
        } else {
            flag = 1;
            cn++;
        }
    }
    str[cn] = '\0';
}

void ME_get_value(unsigned short* var, unsigned short min, unsigned short max, char* cmdbuf) {
    ME_remove_start_blank(cmdbuf);

    char* arg = &cmdbuf[2];
    int temp = 0;
    int rc = sscanf(arg, "%d", &temp);
    if (rc == 1) {
        if ((temp <= (int)max) && (temp >= (int)min)) {
            // TODO(ER) - add lock
            *var = (unsigned short)temp;
            return;
        }
    }

    ME_command_error(ERR_OUT_OF_RANGE_PARM, NULL);
}

