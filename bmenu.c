#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "bmenu.h"
#include "menu.h"


/* Prototypes.*/
void BM_set_prime1(char* cmdbuf);
void BM_set_prime2(char* cmdbuf);
void BM_set_prime3(char* cmdbuf);
void BM_set_delay(char* cmdbuf);

/* Globals.*/
unsigned short prime1;
unsigned short prime2;
unsigned short prime3;
unsigned short delay;


ITEM_LIST BM_prime_number_1[] = {{ITEM_SHORT, (void*)(&prime1), 0, 0, 3, "%03d"},
                                 {ITEM_LAST, NULL, 0, 0, 0, NULL}};


ITEM_LIST BM_prime_number_2[] = {{ITEM_SHORT, (void*)(&prime2), 0, 0, 3, "%03d"},
                                 {ITEM_LAST, NULL, 0, 0, 0, NULL}};


ITEM_LIST BM_prime_number_3[] = {{ITEM_SHORT, (void*)(&prime3), 0, 0, 3, "%03d"},
                                 {ITEM_LAST, NULL, 0, 0, 0, NULL}};


ITEM_LIST BM_delay[] = {{ITEM_SHORT, (void*)(&delay), 0, 0, 3, "%03d"},
                        {ITEM_LAST, NULL, 0, 0, 0, NULL}};


const CMD BM_menu_table[] = {{"A", 0, 0, BM_set_prime1, "1st Prime Number", BM_prime_number_1},
                             {"B", 0, 0, BM_set_prime2, "2nd Prime Number", BM_prime_number_2},
                             {"C", 0, 0, BM_set_prime3, "3rd Prime Number", BM_prime_number_3},
                             {"T", 0, 0, BM_set_delay, "Delay betweem LED on (mSec)", BM_delay},
                             {"?", 0, 0, 0, "Display help", NULL},
                             {0, 0, 0, 0, 0, NULL}};

void BM_default() {
    prime1 = 41;
    prime2 = 43;
    prime3 = 47;
    delay = 250;
}

void BM_set_prime1(char* cmdbuf) {
    ME_get_value(&prime1, 0, 600, cmdbuf);
}

void BM_set_prime2(char* cmdbuf) {
    ME_get_value(&prime2, 0, 600, cmdbuf);
}

void BM_set_prime3(char* cmdbuf) {
    ME_get_value(&prime3, 0, 600, cmdbuf);
}

void BM_set_delay(char* cmdbuf) {
    ME_get_value(&delay, 0, 65535, cmdbuf);
}

