
#include "pico/stdlib.h"
#include "pico/multicore.h"
#include <stdio.h>


#include "menu.h"
#include "bmenu.h"

#include "tst.h"

const uint LED_PIN1 = 25;
const uint LED_PIN2 = 14;
const uint LED_PIN3 = 15;
const uint LED_PIN4 = 10;


void init_IO_pins() {
    gpio_init(LED_PIN1);
    gpio_set_dir(LED_PIN1, GPIO_OUT);

    gpio_init(LED_PIN2);
    gpio_set_dir(LED_PIN2, GPIO_OUT);

    gpio_init(LED_PIN3);
    gpio_set_dir(LED_PIN3, GPIO_OUT);

    gpio_init(LED_PIN4);
    gpio_set_dir(LED_PIN4, GPIO_OUT);
}

void blinkLED() {
    int i = 0;
    while (true) {
        gpio_put(LED_PIN1, 1);

        if ((i % prime1) == 0) {
            gpio_put(LED_PIN2, 1);
        }

        if ((i % prime2) == 0) {
            gpio_put(LED_PIN3, 1);
        }

        if ((i % prime3) == 0) {
            gpio_put(LED_PIN4, 1);
        }

        sleep_ms(delay);

        gpio_put(LED_PIN1, 0);

        if ((i % prime1) == 0) {
            gpio_put(LED_PIN2, 0);
        }

        if ((i % prime2) == 0) {
            gpio_put(LED_PIN3, 0);
        }

        if ((i % prime3) == 0) {
            gpio_put(LED_PIN4, 0);
        }

        sleep_ms(delay);
        i++;
    }
}


int main() {
    BM_default();

    stdio_init_all();

    init_IO_pins();

    multicore_launch_core1(blinkLED);

    while (1) {
        command();
    }
}

