# Raspberry Pico

## LED blink program

The program blinks 3 LED depending on if a counter is modulus a set prime number.

The LED blink function is running on core1 and a command line interface for the serialport is running on core0

The LED used are on pin 25, 14, 15 and 10


## TODO
The global variables are not protected at this point


## Serial output example (minicom)
```
>
>
>?
2021 (C) Blinker inc.

Available Commands:

B? --------------------- B Commands
?? --------------------- Help menu

>b?
2021 (C) Blinker inc.

Available Commands:
BA = 041 ----------------- 1st Prime Number
BB = 043 ----------------- 2nd Prime Number
BC = 047 ----------------- 3rd Prime Number
BT = 250 ----------------- Delay betweem LED on (mSec)
B? = --------------------- Display help

>

```


