#ifndef MENU_H
#define MENU_H





/* ASCII keycodes */
#define CTRLR 18
#define CTRLU 21
#define BS 8
#define TAB 9
#define ESC 27


enum {
    ERR_BADPARM = 1,
    ERR_OUT_OF_RANGE_PARM,
    ERR_BADCMD,
    ERR_CMDCONFLICT,
    ERR_RECDEPOPEN,
    ERR_RECNOEXIST,
};


/* Data types */
enum {
    ITEM_CHAR,
    ITEM_STR,
    ITEM_SHORT,
    ITEM_INT,
    ITEM_LONG,
    ITEM_BYTE_LIST,
    ITEM_SHORT_BIN,
    ITEM_TIME,
    ITEM_DATE,
    ITEM_DATETIME,
    ITEM_LAST
};
/* ITEM_BYTE_LIST Must be null terminated */


/* Item List Structure */
typedef struct il {
    unsigned short data_type;
    void* address;
    unsigned long mask;
    unsigned short shift;
    unsigned short field_len;
    char* fmt;
} ITEM_LIST;

typedef struct cm {
    char* name;
    int expert;
    struct cm* submenu;
    void (*func)();
    char* descrip;
    ITEM_LIST* item_list;
} CMD;

typedef struct {
    long code;
    char* msg;
} CMDERR;

typedef struct {
    char* topname;
    char* name;
} EXC_LIST;


/* Prototypes */
void command();

void ME_command_error(long errcode, char* errstr);
void ME_get_value(unsigned short* var, unsigned short min, unsigned short max, char* cmdbuf);

#endif

